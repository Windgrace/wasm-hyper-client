#![warn(rust_2018_idioms)]
#![allow(unused_imports, dead_code)]
static mut PROMISE_VALUE: Option<Result<ConnectResponse, Box<StdErr>>> = None;
use core::{
    future::Future,
    ops::Deref,
    task::{Context, Poll},
};
use futures_executor::block_on;
use futures_util::future::{ready, Ready};
use serde::{Deserialize, Serialize};
use std::{boxed::Box, env, error::Error as StdError, pin::Pin, time::Duration};

use hyper::{
    body::HttpBody as _,
    client::connect::{Connected, Connection},
    Body, Client, Request, Uri,
};
use tokio::io::{self, AsyncRead, AsyncWrite, AsyncWriteExt as _, Error};
use tower_service::Service;
use wasm_bindgen::{prelude::*, JsCast};
use wasm_bindgen_futures::JsFuture;
use web_sys::{console::log_1, Request as WebRequest, Response};

impl AsyncRead for ConnectResponse {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<io::Result<usize>> {
        Pin::new(&mut self.as_slice()).poll_read(cx, buf)
    }
}

impl AsyncWrite for ConnectResponse {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, Error>> {
        Pin::new(&mut self).poll_write(cx, buf)
    }

    fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        Pin::new(&mut self).poll_flush(cx)
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Error>> {
        Pin::new(&mut self).poll_shutdown(cx)
    }
}

impl Connection for ConnectResponse {
    fn connected(&self) -> Connected {
        Connected::new()
    }
}

#[derive(Clone)]
struct ConnectImplConformer;

impl Service<Uri> for ConnectImplConformer {
    type Response = ConnectResponse;
    type Error = Box<StdErr>;
    type Future = Ready<Result<ConnectResponse, Box<StdErr>>>;

    fn poll_ready(&mut self, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, _dst: Uri) -> Self::Future {
        unsafe {
            match PROMISE_VALUE.take() {
                Some(val) => ready(val),
                None => panic!("You need to await the promise before calling the service!"),
            }
        }
    }
}

type StdErr = dyn StdError + Send + Sync + 'static;

#[wasm_bindgen(start)]
pub async fn start() -> Result<(), JsValue> {
    console_error_panic_hook::set_once();
    log_1(&"Inside start function!".into());

    match serve().await {
        Ok(_) => Ok(()),
        Err(e) => Err(JsValue::from(e.to_string())),
    }
}

#[derive(Clone)]
struct ConnectResponse(Vec<u8>);

impl Deref for ConnectResponse {
    type Target = Vec<u8>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

async fn serve() -> Result<(), Box<StdErr>> {
    let url = "http://127.0.0.1:3030";

    log_1(&"Inside fetch_url!".into());

    async {
        log_1(&"Inside async block!".into());

        let temp_value = Some(get_val_of_promise(url).await);

        log_1(&"Promise evaluated!".into());

        unsafe {
            PROMISE_VALUE = temp_value;
        };

        log_1(&"Assigned promise to static".into());

        let client = Client::builder()
            .pool_idle_timeout(Duration::from_secs(30))
            .http2_only(true)
            .build::<_, Body>(ConnectImplConformer);

        log_1(&"client built!".into());

        let res = client.get(url.parse::<hyper::Uri>().unwrap()).await?;

        log_1(&"performed client.get!".into());

        log_1(&format!("Response: {}", res.status()).into());
        log_1(&format!("Headers: {:#?}\n", res.headers()).into());

        Ok(())
    }
    .await
}

async fn get_response(url: &str) -> Result<JsValue, JsValue> {
    let request = WebRequest::new_with_str(url)?;
    let response = JsFuture::from(web_sys::window().unwrap().fetch_with_request(&request))
        .await?
        .dyn_into::<Response>()
        .unwrap();
    JsFuture::from(response.json()?).await
}

async fn get_val_of_promise(url: &str) -> Result<ConnectResponse, Box<StdErr>> {
    match JsFuture::from(
        web_sys::window()
            .unwrap()
            .fetch_with_request(&WebRequest::new_with_str(url).ok().unwrap()),
    )
    .await
    {
        Ok(m) => match m.dyn_into::<Response>().unwrap().status() {
            n @ 200..=299 => {
                log_1(&n.into());
                Ok(ConnectResponse(Vec::new()))
            }
            e @ _ => {
                log_1(&e.into());
                Err(<Box<StdErr>>::from(format!("Error code: {}", e)))
            }
        },
        Err(e) => Err(<Box<StdErr>>::from(match e.as_string() {
            Some(v) => v,
            None => "No error message found".into(),
        })),
    }
}
